const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'admin',
    password : 'admin',
    database : 'Work-IO'
})

connection.connect()

const express = require('express')
const { hash } = require('bcrypt')
const app = express()
const port = 4000


app.post("/add_employees", (req, res) => {

    let name = req.query.name
    let surname = req.query.surname

    let query = `INSERT INTO Employees 
                    (Name, SurName) 
                    VALUES ('${name}','${surname}')`

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status"    :   "400",
                        "message"   :   "Error inserting data into db"
            })
        }else {
            res.json({
                        "status"    :   "200",
                        "message"   :   "Adding employees succesful"
            })
        }
    })
})

app.post("/list_Working", authenticateToken, (req, res) => {

    let id = req.user.user_id

    let query = "SELECT * From Working"

    connection.query( query, (err, rows) => {
        if (err) {
            res.json( {
                        "status"    :   "400",
                        "message"   :   "Error querying from db"
            })
        }else {
            res.json(rows)
        }
    })
})

app.post("/update_employees", authenticateToken, (req, res) => {

    let id = req.user.user_id
    let name = req.query.name
    let surname = req.query.surname

    let query = `UPDATE Employees SET 
                    Name='${name}',
                    SurName='${surname}'
                    WHERE ID=${id}`

    connection.query( query, (err, rows) => {
        console.log(err)
        if (err) {
            res.json({
                        "status"    :   "400",
                        "message"   :   "Error updating record"
            })
        }else {
            res.json({
                        "status"    :   "200",
                        "message"   :   "Updating employees succesful"
            })
        }
    })
})

app.post("/delete_employees", (req, res) => {

    let id = req.query.id

    let query = `DELETE FROM Employees
                    WHERE ID=${id}`

    connection.query( query, (err, rows) => {
        console.log(err)
        if (err) {
            res.json({
                        "status"    :   "400",
                        "message"   :   "Error deleting record"
            })
        }else {
            res.json({
                        "status"    :   "200",
                        "message"   :   "Deleting record success"
            })
        }
    })
})

app.post("/register", (req, res) => {
    let name = req.query.name
    let surname = req.query.surname
    let username = req.query.username
    let password = req.query.password

    bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {

        let query = `INSERT INTO Employees 
                    (Name, Surname, Username, Password) 
                    VALUES ('${name}','${surname}','${username}','${hash}')`

        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "status"    :   "400",
                            "message"   :   "Error inserting data into db"
                })
            }else {
                res.json({
                            "status"    :   "200",
                            "message"   :   "Adding new user succesful"
                })
            }
        })
    })
})

app.post("/login", (req, res) => {

    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Employees WHERE Username='${username}'`

    connection.query( query, (err, rows) => {
        if (err) {
            res.json( {
                        "status"    :   "400",
                        "message"   :   "Error querying from running db"
            })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].ID
                    }
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' })
                    res.send(token)
                }else{
                    res.send("Invalid username / password")
                }
            })
            
        }
    })
})

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) {
            return res.sendStatus(403)
        }
        else {
            req.user = user
            next()
        }
    })
}

app.listen(port, () => {
    console.log(`Now starting Work-IO Backend ${port}`)
})